
def handle_merge_request(gitlab, request, options):
    # Get the project from webhook information
    project = gitlab.projects.get(request['object_attributes']['source_project_id'])

    # Get the MR from project+webhook information
    mr = project.mergerequests.get(request['object_attributes']['iid'])

    if "closes_issue" in options:
        # If the MR closes any issues, update their labels as defined
        for issue in mr.closes_issues():

            for label in options["closes_issue"]["add_labels"]:
                if label not in issue.labels:
                    issue.labels.append(label)

            for label in options["closes_issue"]["remove_labels"]:
                if label in issue.labels:
                    issue.labels.remove(label)

            issue.save()

    return "OK"
