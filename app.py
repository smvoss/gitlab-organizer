import gitlab
import json

from flask import request
from flask_api import FlaskAPI
from events import events

app = FlaskAPI(__name__)


@app.route("/merge_request_event", methods=['GET', 'POST'])
def merge_request_event():
    if request.method == 'POST':
        # Get the request
        gl_request = request.get_json('text', '')

        # Get the function name (also event name)
        name = request.url_rule.endpoint

        # Check if state has a defined action in config file
        state = gl_request['object_attributes']['state']
        if state in OPTIONS[name]:
            events.handle_merge_request(
                GITLAB,
                gl_request,
                OPTIONS[name][state]
            )
        else:
            print("{event}.{state} not defined in options.json".format(
                event=name, state=state)
            )

        return "OK"

    else:
        return "This was a GET request"


if __name__ == "__main__":
    # load the default gitlab configuration
    GITLAB = gitlab.Gitlab.from_config("gitlab", ['config/gitlab.cfg'])

    # load app options
    with open('config/options.json') as app_options:
        OPTIONS = json.load(app_options)

    app.run(host='0.0.0.0')
