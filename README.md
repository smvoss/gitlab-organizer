# Gitlab Organizer

Gitlab organizer strives to add QOL updates to make moving tickets around the
issue tracker/board easier by assigning labels based on events happening.

NOTE: Gitlab Organizer is in very early development and has very little implemented.

# Configuration

`options.json` is a configuration file based on a web hook -> state -> action
workflow.

The root string (ex `merge_request_event`) specifies the type of event 
(web hook) the configuration acts against. The next level (ex `opened`) is the
"state" of the request, and all children are either an "object"
(ex `closes_issue`) or an "action" (ex `add_label`).

An object's children are actions.

## Objects

| Event | Object | Description |
| ----- | ------ | ----------- |
| Merge Request | closes_issue | The issue (or issues) that will be closed with the merge request 


## Actions

| Action | Type | Description |
| ----- | ------ | ----------- |
| add_labels | Array | Add all specified labels to the parent object
| remove_labels | Array | Remove all specified labels of the parent object

# Examples

## Add labels to a merge request's "closes issues"

* Add the "Test/Review" label to the all of the issues the merge request will
close.

* A ticket getting assigned the "Test/Review" label should no longer
have a "Implement" or "Blocked" label.

### Example Configuration
```json
{
  "merge_request_event": {
    "opened": {
      "closes_issue": {
        "add_label": "Test/Review",
        "remove_labels": [
          "Implement",
          "Blocked"
        ]
      }
    }
  }
}
```

### Expected Behavior
1. A merge request is opened that will close an issue 
2. Gitlab ticket API is queried to get issue status, including current labels
3. Label "Test/Review" is added, and labels "Implement" and "Blocked" are
removed if they exist.